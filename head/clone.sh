#!/bin/bash
# exit on ERROR
set -e

#
# ######################################################
# Prerequisite
# ######################################################
# REF: http://wiki.pitivi.org/wiki/GStreamer_from_Git
# Install development tool
# Install build dependencies
# Add ~/bin in your path

MYGST=$HOME/work/gstreamer

# ######################################################
# Download source
# ######################################################
[ ! -d "$MYGST/head" ] && mkdir -p $MYGST/head
cd $MYGST/head
# modules to be downloaded
GST_MOD=(\
"gstreamer 1.4.5" \
"gst-plugins-base 1.4.5" \
"gst-plugins-good 1.4.5" \
"gst-ffmpeg 1.4.5" \
"gst-rtsp-server 1.4.5" \
#"gst-plugins-bad 1.4.5" \
#"gst-plugins-ugly 1.4.5" \
#"gnonlin 1.4.0" \
#"gst-python 1.4.0" \
)
for moddata in "${GST_MOD[@]}"
do
	modinfo=(${moddata[*]}) # unquotate
	mod="" && [ ${#modinfo[*]} -gt 0 ] && mod=${modinfo[0]}
	ver="" && [ ${#modinfo[*]} -gt 1 ] && ver=${modinfo[1]}

	if [ ! -e "$mod" ] ; then
		echo ">>> $mod: Download source..."
	 	git clone git://anongit.freedesktop.org/gstreamer/$i "$i"
	elif [ -d "$mod" ] ; then
		echo ""
		#echo ">>> $mod: Fetch source..."
		#git fetch
	fi
	if [ "$ver" != "" ] ; then
		pushd $mod
		echo ">>> $mod: Checkout to $ver..."
		git checkout $ver
		popd
	fi
	echo "==========================================="
done


#DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
#MYGST="$DIR/gstreamer.git"
if [ ! -e "$HOME/bin/gst-head" ] ; then
	cp $MYGST/head/gstreamer/scripts/gst-uninstalled ~/bin/gst-head

	echo ">>> Handy job is required. !!!"
	echo "gst-head's been copied to ~/bin..."
	echo "Open the ~/bin/gst-head and set MYGST to $MYGST as following:"
	echo "MYGST=$MYGST"
	# TODO: replace MYGST
	# sed -i 's/MYGST=/MYGST=$MYGST/g' ~/bin/gst-head
fi

#
# Compile
#
echo "Ready to compile gstreamer"
echo "Do the following jobs:"
echo '$>gst-head'
echo '$>cd module_name'
echo '$>./autogen.sh'
echo '$>make -j2'
echo '$>exit'
echo "Modules:${GST_MOD[@]}"

# for moddata in "${GST_MOD[@]}"
# do
# 	modinfo=(${moddata[*]}) # unquotate
# 	mod="" && [ ${#modinfo[*]} -gt 0 ] && mod=${modinfo[0]}
# 	#ver="" && [ ${#modinfo[*]} -gt 1 ] && ver=${modinfo[1]}
# 	gst-head
# 	if [ -d "$mod" ] ; then
# 		cd $mod
# 		echo ">>> $mod: compile"
# 	fi
# 	echo "==========================================="
# done

# for i in ${GST_MOD[*]}
# do
# 	cd "$i.git"
# 	./autogen.sh
# 	make
# 	cd ..
# 	gst-head
# done

