#!/bin/bash
# exit on ERROR
set -e

if [ "$1" == "" ] || [ "$GST_PLUGIN_PATH" == "" ] ; then
	echo -e "Usage: \t1. gst-head
\t2. $0 \"mod_name [version]\"
\t3. exit"
	exit 1
fi

INSTDIR=$HOME/gst-build
BINDIR=$INSTDIR/bin

moddata=$1
modinfo=(${moddata[*]}) # unquotate
mod="" && [ ${#modinfo[*]} -gt 0 ] && mod=${modinfo[0]}
ver="" && [ ${#modinfo[*]} -gt 1 ] && ver=${modinfo[1]}

if [ ! -d "$mod" ] ; then
	echo ">>> $mod: Download source..."
	git clone git://anongit.freedesktop.org/gstreamer/$i "$i"
else
	echo ""
	#echo ">>> $mod: Fetch source..."
	#git fetch
fi

if [ "$ver" != "" ] ; then
	pushd $mod
	echo ">>> $mod: Checkout to $ver..."
	git checkout $ver
	popd
fi

echo "==========================================="
echo ">>> Building $mod"

cd $mod
./autogen.sh --prefix=$INSTDIR --bindir=$BINDIR
make -j2

echo "==========================================="
echo "Finished..."
echo "type 'exit'"
